### Master's degree thesis

Implementation of an EEG signal processing pipeline for Brain Computer Interface applications.

The pipeline is implemented using [MNE_Python](https://mne.tools/stable/index.html).

### Supported datasets

| Dataset | Experimental Paradigm | Number of Classes |
| --- | --- | --- |
| [Physionet MM/MI Dataset](https://physionet.org/content/eegmmidb/1.0.0/) | Motor Movement/ Motor Imagery | 8 (4 MM + 4 MI)|
| [BCI competition IV - Dataset 2A](http://www.bbci.de/competition/iv/#dataset2a) | Motor Imagery | 4 |
| [BCI competition IV - Dataset 2B](http://www.bbci.de/competition/iv/#dataset2b) | Motor Imagery | 2 |
