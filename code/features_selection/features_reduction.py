# Features reduction module (PCA/LDA)


from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
import matplotlib.pyplot as plt
import numpy as np


# Define function to plot explained variance ratio for each component
def plot_expl_var_ratio(projector, name) :
    # Plot explained variance
    plt.figure()
    plt.ylabel('% Explained Variance')
    plt.xlabel('# of Features')
    plt.ylim([0, 1.1])
    #plt.xlim([0, n_classes])
    plt.title(name+' Analysis')
    plt.grid()
    plt.plot(projector.explained_variance_ratio_)
    plt.plot(np.cumsum(projector.explained_variance_ratio_))
    plt.legend(['Expl. var. ratio', 'Comulative expl. var.'])
    
    
def plot_features_distribution(X_train, y_train, n_components, name) :
    # Number of classes
    n_classes = len(np.unique(y_train))
    # Instances distribution in PC_0-PC_1/LD_0-LD_1 space
    fig = plt.figure(figsize = (8,8))
    ax = fig.add_subplot(1,1,1) 
    ax.set_xlabel(name+' 0', fontsize = 15)
    ax.set_ylabel(name+' 1', fontsize = 15)
    ax.set_title('2 component '+name, fontsize = 20)
    targets = np.arange(0, n_classes)
    colors = ['r', 'b']
    if (n_classes > 2) :
        colors += ['g', 'y']
    # Plot data
    for target, color in zip(targets, colors):
        indicesToKeep = y_train.values == target
        ax.scatter(X_train[indicesToKeep, 0],
                   X_train[indicesToKeep, 1],
                   c = color,
                   s = 50)
    ax.legend(targets)
    ax.grid()


# Defining function to compute PCA
def compute_PCA(X_train, X_test, y_train, y_test, plot):
    # Inspecting PCs to select most discriminant features
    if (plot) :
        pca = PCA(min(X_train.shape[0], X_train.shape[1]))
        pca.fit(X_train)
        plot_expl_var_ratio(pca, 'PCA')       
    
    # Selecting first 2 PCs (which explain more than 90% variance)
    pca = PCA(n_components=2)
    X_train = pca.fit_transform(X_train)
    X_test = pca.transform(X_test)
    
    # Printing first 2 PCs distribution
    if (plot):
        plot_features_distribution(X_train, y_train, pca.n_components, 'PCA')
        
    return X_train, X_test, y_train, y_test


# Defining function to compute LDA
def compute_LDA(X_train, X_test, y_train, y_test, plot):
    # Number of classes
    n_classes = len(np.unique(y_test))
    # Inspecting PCs to select most discriminant features
    if ((n_classes>2) & plot) :
        lda = LDA(n_components = n_classes-1)
        lda.fit(X_train, y_train)
        plot_expl_var_ratio(lda, 'LDA')
    
    # Selecting LDs
    lda = LDA(n_components=n_classes-1)
    X_train = lda.fit_transform(X_train, y_train)
    X_test = lda.transform(X_test)
    
    # Plot LDA distribution
    if ((n_classes>2) & plot) :
        plot_features_distribution(X_train, y_train, lda.n_components, 'LDA')
    
    return X_train, X_test, y_train, y_test
    

# Apply features reduction (PCA/LDA/None)
def features_reduction(X_train, X_test, y_train, y_test, features_reduction_technique, plot_features_reduction) :
    # Computing PCA
    if (features_reduction_technique == 'PCA') :
        return compute_PCA(X_train, X_test, y_train, y_test, plot_features_reduction)
    # Computing PCA
    elif (features_reduction_technique == 'LDA') :
        return compute_LDA(X_train, X_test, y_train, y_test, plot_features_reduction)
    # No features reduction
    print('NO FEATURES REDUCTION APPLIED')
    return X_train, X_test, y_train, y_test