# Extract correlation between channels

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn
from scipy import stats # pearson/spearman correlation coefficients
from joblib import Parallel, delayed # parallel computation of corr_mtx coefficients


# Compute Pearson coefficient
def compute_pearson_coeff(data, n_ch):
    mtx = np.empty([n_ch, n_ch])
    for ch_i in range(n_ch):
            for ch_j in range(n_ch):
                mtx[ch_i, ch_j], _ = stats.pearsonr(data[ch_i, :], data[ch_j, :])
    return mtx


# Compute Spearman coefficient
def compute_spearman_coeff(data, n_ch) :
    mtx = np.empty([n_ch, n_ch])
    for ch_i in range(n_ch):
            for ch_j in range(n_ch):
                mtx[ch_i, ch_j], _ = stats.spearmanr(data[ch_i, :], data[ch_j, :])
    return mtx


# Plot average correlation matrix
def plot_correlation_matrix(corr_mtx, task, correlation) :
    plt.figure()
    sn.heatmap(corr_mtx)
    plt.title(task + " - " + correlation)
    plt.show()


# Compute correlation among electrodes using Pearson/Spearman correlation index
def compute_corr_mtx(e, task, correlation, plot_mtx) :
    # Number of instances
    n_instances = e._data.shape[0]
    # Number of electrodes
    n_ch = len(e.ch_names)
    # Correlation matrix
    corr_mtx = np.zeros((n_instances, n_ch, n_ch))
    #p_val = np.zeros((n_instances, n_ch, n_ch))
    
    # Parallel computation of correlation coefficient
    # Pearson correlation
    if (correlation == "pearson") :
        corr_mtx  = Parallel(n_jobs=-1)(delayed(compute_pearson_coeff)
                        (e._data[inst, :, :], n_ch) for inst in range(e._data.shape[0]))
    # Spearman correlation
    else :
        corr_mtx  = Parallel(n_jobs=-1)(delayed(compute_spearman_coeff)
                        (e._data[inst, :, :], n_ch) for inst in range(e._data.shape[0]))    
    
    # Compute average correlation matrix 
    corr_mtx = np.sum(corr_mtx, axis=0)
    corr_mtx /= n_instances
    
    # Visualize average correlation matrix
    if (plot_mtx) :
        plot_correlation_matrix(corr_mtx, task, correlation)    
    
    return corr_mtx


# Filter channels according to their correlation
def filter_channels(corr_mtx, n_ch) :
    f_ch = set()
    for i in range(n_ch) :
        j = 0
        # Iterating only on the lower half matrix
        while (i >= j):
            # Pearson correlation threshold set to 0.3
            if (np.abs(corr_mtx[i,j]) < 0.3):
                f_ch.add(i)
                f_ch.add(i)
            j += 1  # Next channels pair   
    return f_ch


# Update filtered channels list
def update_channel_list(e, task, channel_filtering_method, plot_mtx) :
    # Computing average correlation matrix between channels
    corr_mtx = compute_corr_mtx(e, task, channel_filtering_method, plot_mtx)
    
    # Selecting most uncorrelated channels indices
    return filter_channels(corr_mtx, len(e.ch_names))


# Extract channels according to channel filtering method
def extract_channel_list(channel_filtering_method, epochs, plot_mtx) :
    # Pearson or Spearman correlation index
    if ((channel_filtering_method == "pearson") | (channel_filtering_method == "spearman")) :  
        # Selecting most uncorrelated channels indices
        filtered_channels = set()
        for task, e in enumerate(epochs) :
            filtered_channels.update(update_channel_list(e, ('task_'+str(task)), 
                                                         channel_filtering_method, 
                                                         plot_mtx))
        # Selecting channel names
        filtered_channels = np.take(epochs[0].ch_names, list(filtered_channels))

    # Motor channels
    elif (channel_filtering_method == "motor_ch") :
        filtered_channels = ['C3', 'C4', 'Cz']
    
    # All channels
    else :
        print("PICKING ALL THE CHANNELS")
        filtered_channels = epochs[0].ch_names
    
    return filtered_channels


# Load filtered channels according to classification problem and
# channel filtering method
def load_filtered_channels(runs, classification_pb, channel_filtering_method) :
    filtered_channels = []
    
    # Multiclass problem  [Motor execution, Motor imagery]
    # Hands_MM vs Hands_MI
    if (runs==[[3, 7, 11], [4, 8, 12]]) :
        # Pearson method
        if (channel_filtering_method == 'pearson') :
            filtered_channels = ['T9', 'T10', 'TP7', 'P7', 'P5', 'P3', 'P1', 'PO7',
                                 'PO3', 'POz', 'PO4', 'PO8', 'O1', 'Oz', 'O2', 'Iz']    
        # Spearman method
        elif (channel_filtering_method == 'spearman') :
            filtered_channels = ['T9', 'T10', 'TP7', 'TP8', 'P7', 'P5', 'P3', 'P1', 
                                 'Pz', 'P2','P6', 'P8', 'PO7', 'PO3', 'POz', 'PO4', 
                                 'PO8', 'O1', 'Oz', 'O2', 'Iz']
    # Hands vs feet (MM)
    elif (runs==[[5, 9, 13], [6, 10, 14]]) :
        # Pearson method
        if (channel_filtering_method == 'pearson') :
            filtered_channels = ['PO7', 'PO3', 'POz', 'PO4', 'PO8', 
                                 'O1', 'Oz', 'O2', 'Iz']    
        # Spearman method
        elif (channel_filtering_method == 'spearman') :
            filtered_channels = ['T10', 'P7', 'P5', 'P3', 'P1', 'P8', 'PO7', 
                                 'PO3', 'POz', 'PO4', 'PO8', 'O1', 'Oz', 'O2', 'Iz']
    
    # Motor execution (MM)
    # Hands (MM)
    elif (runs==[[3, 7, 11]]) :
        # Pearson executed
        if (channel_filtering_method == 'pearson') :
            filtered_channels = ['T9', 'PO7', 'PO8', 'O1', 'Oz', 'O2', 'Iz']    
        # Spearman executed
        elif (channel_filtering_method == 'spearman') :
            filtered_channels = ['T9', 'T10', 'P7', 'PO7', 'PO3', 'POz', 
                                 'PO4', 'PO8', 'O1', 'Oz','O2', 'Iz']
    # Hands vs feet (MM)
    elif (runs==[[5, 9, 13]]) :
        # Pearson executed
        if (channel_filtering_method == 'pearson') :
            filtered_channels = ['O1', 'Oz', 'O2', 'Iz']    
        # Spearman executed
        elif (channel_filtering_method == 'spearman') :
            filtered_channels = ['PO7', 'PO8', 'O1', 'Oz', 'O2', 'Iz']
                
    # Motor imagery (MI)
    # Hands (MI)
    elif (runs==[[4, 8, 12]]) :
        # Pearson imagined
        if (channel_filtering_method == 'pearson') :
            filtered_channels = ['T9', 'T10', 'TP7', 'P7', 'P5', 'P3', 'P1', 'PO7', 
                                 'PO3', 'POz', 'PO4', 'PO8', 'O1', 'Oz', 'O2', 'Iz']    
        # Spearman imagined
        elif (channel_filtering_method == 'spearman') :
            filtered_channels = ['T9', 'T10', 'TP7', 'TP8', 'P7', 'P5', 'P3', 'P1', 
                                 'Pz', 'P4', 'P8', 'PO7', 'PO3', 'POz', 'PO4', 
                                 'PO8', 'O1', 'Oz', 'O2', 'Iz']
    # Hands vs feet (MI)
    elif (runs==[[6, 10, 14]]) :
        # Pearson imagined
        if (channel_filtering_method == 'pearson') :
            filtered_channels = ['PO7', 'PO3', 'POz', 'PO4', 'PO8', 
                                 'O1', 'Oz', 'O2', 'Iz']    
        # Spearman imagined
        elif (channel_filtering_method == 'spearman') :
            filtered_channels = ['T10', 'P7', 'P5', 'P3', 'P1', 'P8', 'PO7', 
                                 'PO3', 'POz', 'PO4', 'PO8', 'O1', 'Oz', 'O2', 'Iz']
                
    return filtered_channels

    
