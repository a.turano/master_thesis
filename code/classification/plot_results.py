# Module to plot classification results (parameters, confusion matrices, ...)

import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import os


# Defining function to print confusion matrix
def print_conf_mtx(conf_mtx, model, save_conf_mtx=False) :
    fig, ax = plt.subplots(figsize=(2.5, 2.5))
    ax.matshow(conf_mtx, cmap=plt.cm.Blues, alpha=0.3)   
    for i in range(conf_mtx.shape[0]):
        for j in range(conf_mtx.shape[1]):
            ax.text(x=j, y=i, s=conf_mtx[i,j], va='center', ha='center')           
    plt.xlabel('Predicted Label')
    plt.ylabel('True Label')
    plt.title(model+' Confusion Matrix')
    plt.show()
    
    return fig
    
    #if (save_conf_mtx()):
    #    path = os.getcwd() + "/../Results/" + dataset + "/" + classification_pb
    #    conf_mtx.savefig(path+'/_CNN_confmtx.png')


# Compute accuracy
def compute_accuracy(conf_mtx) :
    return round((sum(conf_mtx.diagonal()))/sum(sum(conf_mtx)), 2)


# Compute confusion matrix and accuracy for each model
def plot_save_results(y_test, predicted_labels, results, channel_filtering_method, sbj, results_single, print_confmat) :
    # Plot confusion matrix and print accuracy for each model
    for model in predicted_labels.keys() :
        # Confusion matrix
        conf_mtx = confusion_matrix(y_test, predicted_labels[model])
        if (print_confmat) :
            print_conf_mtx(conf_mtx, model)
        
        # Accuracy
        accuracy = compute_accuracy(conf_mtx)
        print(model+'_accuracy = '+str(accuracy))
        
        # Storing results in dictionary
        results[channel_filtering_method][model] += accuracy
        
        # Store single subject results
        if (len(sbj)==1) :
            results_single['sbj'].append(sbj[0])
            results_single['ch_sel'].append(channel_filtering_method)
            results_single['model'].append(model)
            results_single['accuracy'].append(accuracy)