# Module to train the classifiers

import numpy as np
from sklearn.model_selection import GridSearchCV, StratifiedKFold
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier as knn
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.initializers import GlorotUniform
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, Dropout
from .save_results import save_model


# Classification using SVM
def svm_classification(X_train, X_test, y_train, y_test, skf, save_models, dataset, classification_pb, 
                       channel_filtering_method) :
    #'''
    c_range = [0.01, 0.1, 1.0, 10.0]
    param_grid = [{'C' : c_range, 'kernel' : ['linear']},
                  {'C' : c_range, 'kernel' : ['rbf']}   # gamma = 1/n_features*var(X)  <- default
                 ]
    #'''
    # Defining parameters for grid oprimization
    #param_grid = [{'C' : [1.0], 'kernel' : ['rbf']}]        
    # Grid search method to select best parameters
    grid = GridSearchCV(estimator=svm.SVC(), param_grid=param_grid, 
                        scoring='accuracy', cv=skf, n_jobs=len(c_range))
    # Training SVM classifier using best parameters
    svm_model = grid.fit(X_train, y_train)        
    # Testing SVM model
    y_pred_svm = svm_model.predict(X_test)
    # Save the model
    if (save_models) :
        save_model(svm_model, 'svm', dataset, classification_pb, channel_filtering_method)
    
    return y_pred_svm


# Classification using KNN
def knn_classification(X_train, X_test, y_train, y_test, skf, save_models, dataset, classification_pb, 
                       channel_filtering_method) :
    #'''
    param_grid = [{'n_neighbors' : [3]},
                  {'n_neighbors' : [5]}, 
                  {'n_neighbors' : [7]}, 
                  #{'n_neighbors' : [9]},
                  {'n_neighbors' : [11]},
                  {'n_neighbors' : [21]},
                  {'n_neighbors' : [31]}
                  ]
    #'''
    # Defining parameters for grid oprimization
    #param_grid = [{'n_neighbors' : [13]}]
    # Grid search method to select best parameters
    grid = GridSearchCV(estimator=knn(), param_grid=param_grid, 
                        scoring='accuracy', cv=skf, n_jobs=len(param_grid))
    # Training KNN classifier using best parameters
    knn_model = grid.fit(X_train, y_train)
    # Testing KNN model
    y_pred_knn = knn_model.predict(X_test)
    # Save the model
    if (save_models) :
        save_model(knn_model, 'knn', dataset, classification_pb, channel_filtering_method)
    
    return y_pred_knn


# Define function to build the NN
def build_NN(X_train, y_train, initializer, activation_function) :
    # Number of classes
    n_classes = len(np.unique(y_train))    
    # Building model
    nn = Sequential()
    nn.add(Dense(64, input_shape=(X_train.shape[1],), 
                 activation = activation_function, 
                 kernel_initializer=initializer))
    nn.add(Dropout(0.2))
    nn.add(Dense(32, activation = activation_function, 
                 kernel_initializer=initializer))
    nn.add(Dropout(0.2))
    # Add last layer and compile the model
    if (n_classes>2) :
        nn.add(Dense(n_classes, activation = 'softmax'))
        nn.compile(loss = 'SparseCategoricalCrossentropy', optimizer='adam', 
                   metrics=['accuracy'])
    else:
        nn.add(Dense(1, activation='sigmoid'))
        nn.compile(loss = 'binary_crossentropy', optimizer='adam', 
                   metrics=['accuracy'])
    # Print NN architecture
    #nn.summary()
    return nn


# Classification using MLP
def mlp_classification(X_train, X_test, y_train, y_test, save_models, dataset, classification_pb, 
                       channel_filtering_method) :
    # Defining early stop function
    early_stop = EarlyStopping(monitor='val_loss', mode='min', patience=30)    
    # Random weights
    initializer = GlorotUniform(seed=42)      
    # Building NN model
    mlp_model = build_NN(X_train, y_train, initializer, 'tanh')
    # Training NN
    mlp_model.fit(X_train, y_train, epochs=300, verbose=0, 
                              validation_split=0.3, callbacks=[early_stop])
                              # batch_size=64
    # Testing NN model
    y_pred_mlp = mlp_model.predict_classes(X_test)
    
    # Save the model
    if (save_models) :
        save_model(mlp_model, 'mlp', dataset, classification_pb, channel_filtering_method)
        
    return y_pred_mlp


def fit_model(X_train, X_test, y_train, y_test, skf, model, save_models, dataset, classification_pb, 
              channel_filtering_method) :
    if (model == 'svm') :
        return svm_classification(X_train, X_test, y_train, y_test, skf, 
                                  save_models, dataset, classification_pb, channel_filtering_method)
    if (model == 'knn') :
        return knn_classification(X_train, X_test, y_train, y_test, skf, 
                                  save_models, dataset, classification_pb, channel_filtering_method)
    if (model == 'mlp') :
        return mlp_classification(X_train, X_test, y_train, y_test, 
                                  save_models, dataset, classification_pb, channel_filtering_method)


# Run all the classifiers
def run_classifiers(X_train, X_test, y_train, y_test, save_models, dataset, 
                    classification_pb, channel_filtering_method, random_state) :
    # Defining stratified k-Fold iterator
    skf = StratifiedKFold(n_splits=3, shuffle=True, random_state=random_state)
    # Defining models to use for experiments
    models = ['svm', 'knn', 'mlp']
    
    # Fitting and testing the models
    results = {}
    for m in models :
        results[m] = fit_model(X_train, X_test, y_train, y_test, skf, m, 
                               save_models, dataset, classification_pb, 
                               channel_filtering_method)
    
    return results
    