# Save restults in a csv file

import pandas as pd
import numpy as np
import os
from joblib import dump  # save models


# Save models to disk
def save_model(model, name, dataset, classification_pb, channel_filtering_method) :
    path = os.getcwd() + "/../Results/" + dataset + "/" + classification_pb + "/" + channel_filtering_method
    # Check if directory exists or not
    if (not os.path.exists(path)) :
        os.makedirs(path)
    
    # Save Sklearn model
    if (type(model).__name__ == 'GridSearchCV') :
        dump(model, path+"/"+name+'_model.sav')
        
        # Save best parameters
        file = open(path + '/' + 'best_params.txt', 'a') 
        file.write(name + ':  ')
        for p in model.best_params_.keys() :
            file.write('Best ' + p + '=' + str(model.best_params_[p]) + '  ')
        file.write('\n')
        file.close()
        
    # Save Keras model
    elif (type(model).__name__ == 'Sequential') :
        model.save( path+"/"+name+'_model.h5')


# Save group results in a csv file
def save_group_results(results, dataset, experiments, classification_pb, channel_filtering_method, single_avg='') :
    results_df = pd.DataFrame.from_dict(results)
    path = os.getcwd() + "/../Results/" + dataset + "/" + classification_pb
    # Check if directory exists or not
    if (not os.path.exists(path)) :
        os.makedirs(path)
    # Save file
    out = path + "/out_"+ single_avg + experiments + "_" + classification_pb + ".csv"
    results_df.to_csv(out, index=False)
    
    # Single experiments require DataFrame and path to be retourned
    # in order to compute the average results for each model
    if (experiments == 'single') :
        return results_df, path
    

# Save single result in a csv file
def save_single_results(results_tot, dataset, experiments, classification_pb, results_single) :    
    # Save single subject results
    results_df, path = save_group_results(results_single, dataset, experiments, classification_pb)

    # Compute average single subject results
    for ch in results_df['ch_sel'].unique() :
        for m in results_df['model'].unique() :
            results_tot[ch][m] = np.mean(results_df.loc[(results_df['ch_sel'] == ch) & 
                                        (results_df['model'] == m)])['accuracy']
    # Save average single subject results
    _, _ = save_group_results(results_tot, dataset, experiments, classification_pb, 'avg_')
    