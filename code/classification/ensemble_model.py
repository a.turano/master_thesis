# Module to define the deep learning model

import numpy as np
#from tensorflow import keras
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.initializers import GlorotUniform
from tensorflow.keras import Sequential, optimizers
from tensorflow.keras.layers import Input, Conv2D, MaxPooling1D, Dense, Dropout, Flatten
from .base_model import BaseModel
import re


# Define supermodel
class EnsembleModel():
    def __init__(self, pred_weights={'frontal_weight':.25, 'temporal_weight':.25, 
                                     'central_weight':.25, 'occipital_weight':.25}) :
        
        self.model = {'frontal_model'   : None,
                      'temporal_model'  : None,
                      'central_model'   : None,
                      'occipital_model' : None
                      }
        self.pred_weights = pred_weights
    
    
    # Build ensemble model
    def build_ensemble_model(self, channels_list, n_samples, n_classes) :
        # Divide channels according to their region of the brain
        channels_subset = self.get_channels_sets(channels_list)
        
        # Define and build a model for each different region of the brain
        for region in ['frontal', 'temporal', 'central', 'occipital'] :
            base_model = BaseModel(channels_subset[region+'_channels'])
            base_model.build_base_model(n_samples, 2)#n_classes)
            self.model[region+'_model'] = base_model
            
    # Fit the ensemble model
    def fit_ensemble(self, X_train, y_train) :
        # Fitting independently each model of the ensamble using a specific
        # subset of channels for each one of them
        for m in self.model.keys() :
            # Select as training set only the selected channels for the model
            X_train_model = X_train[:, self.model[m].channels_subset, :]
            X_train_model = X_train_model[..., np.newaxis]

            print('FITTING ' + m + ' MODEL...')
            self.model[m].fit_base_model(X_train_model, y_train)
            print('...FINISH ' + m + ' MODEL TRAINING')
            
        
    # Predict test labels
    def predict(self, X_test) :
        probs = 0
        # Multiply each prediction for its weight according to the specific brain region
        for m, w in zip(self.model.keys(), self.pred_weights.keys()) :
            X_test_model = X_test[:, self.model[m].channels_subset, :]
            X_test_model = X_test_model[..., np.newaxis]

            # Sum of weighted probabilities
            probs += self.pred_weights[w] * self.model[m].predict(X_test_model)            
        # Return predicted labels
        return np.round(probs)
        
        
    # Divide channels according to their region of the brain
    def get_channels_sets(self, channels_list) :
        # Divide channels and extract channel indices
        # set(ch for ch in channels_list for i in ch if(i in ['Fp', 'AF', 'F']))
        tmp_ch = ['Fp1', 'Fpz', 'Fp2', 'AF7', 'AF3', 'AFz', 'AF4', 'AF8',  
                  'F7','F5', 'F3', 'F1', 'Fz', 'F2', 'F4', 'F6', 'F8']
        frontal_channels   = set(ch for ch in channels_list if(ch in tmp_ch))
        frontal_channels = np.where(np.isin(channels_list, list(frontal_channels)))[0]
        
        # ['FT', 'FC', 'T', 'TP', 'CP']
        tmp_ch = ['FC5', 'FC3', 'FC1', 'FCz', 'FC2', 'FC4', 'FC6', 'CP5', 'CP3', 'CP1', 'CPz',
                  'CP2', 'CP4', 'CP6', 'FT7', 'FT8', 'T7', 'T8', 'T9', 'T10', 'TP7', 'TP8']
        temporal_channels  = set(ch for ch in channels_list if(ch in tmp_ch))
        temporal_channels = np.where(np.isin(channels_list, list(temporal_channels)))[0]
        
        # ['C']
        tmp_ch = ['C5', 'C3', 'C1', 'Cz', 'C2', 'C4', 'C6']
        central_channels   = set(ch for ch in channels_list if(ch in tmp_ch))
        central_channels = np.where(np.isin(channels_list, list(central_channels)))[0]
        
        # ['P', 'PO', 'O', 'Iz']
        tmp_ch = ['P7', 'P5', 'P3', 'P1', 'Pz', 'P2', 'P4', 'P6', 'P8', 'PO7',
                  'PO3', 'POz', 'PO4', 'PO8', 'O1', 'Oz', 'O2', 'Iz']
        occipital_channels = set(ch for ch in channels_list if(ch in tmp_ch))
        occipital_channels = np.where(np.isin(channels_list, list(occipital_channels)))[0]
        
        # Return channel indices
        return {'frontal_channels'  : frontal_channels, 
                'temporal_channels' : temporal_channels, 
                'central_channels'  : central_channels, 
                'occipital_channels': occipital_channels
                }

    
    # Set weights for prediction of each model
    def set_pred_weights(self, pred_weights) :
        # Check if weights sum to 1
        if (sum(pred_weights) == 1) :
            self.pred_weights = pred_weights
        else :
            print("ERROR: weights must sum to 1. They have been setted to default values")
