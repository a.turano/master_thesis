# Class for the base CNN model

import matplotlib.pyplot as plt
import tensorflow
from tensorflow.keras.callbacks import EarlyStopping, LearningRateScheduler, ModelCheckpoint
from tensorflow.keras.initializers import GlorotUniform
from tensorflow.keras import Sequential, optimizers, regularizers
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dense, Dropout, Flatten


class BaseModel() :
    
    def __init__(self, channels_subset, path=None) :
        self.channels_subset = channels_subset
        self.n_channels = len(self.channels_subset)  # number of channels
        self.model = None
        self.network_history = None
        self.params={'initializer'     : GlorotUniform(seed=42),  # random weights
                     'activation'      : 'relu',  # activation function
                     'binary_loss'     : 'binary_crossentropy',  # loss function
                     'multiclass_loss' : 'SparseCategoricalCrossentropy',  # multiclass loss function
                     'optimizer'       : optimizers.Adam(), #  optimizer
                     'metrics'         : ['accuracy'],  # metrics
                     'dropout_rate'    : 0.2,  # dropout rate
                     'early_stop'      : EarlyStopping(monitor='val_loss', mode='min', patience=30),  # early stopping
                     'adaptative_lr'   : LearningRateScheduler(scheduler, verbose=1),
                     'checkpoint'      : ModelCheckpoint(path+'/checkpoint/best_model.hdf5', monitor='val_accuracy', verbose=1, save_best_only=True, mode='max')
                     }
        
    # Build base model for each subset of channels
    def build_base_model(self, n_samples, n_classes) :
        # Building model
        model = Sequential()
        # shape = 1 x n_samples x n_ch
        #   - img -> 2 dim con depth 3
        #   - EEG -> 1 dim con depth n_ch
        #model.add(Input(shape=(self.n_channels, n_samples)))
        #model.add(Input(shape=(n_samples, self.n_channels)))
        input_shape = (self.n_channels, n_samples, 1)
        
        # Temporal convolution
        model.add(Conv2D(filters=8, kernel_size=(1, 3), activation=self.params['activation'],
                         input_shape=input_shape,
                         kernel_initializer=self.params['initializer'],
                         kernel_regularizer=regularizers.l2(1e-4), 
                         name='Temporal_convolution'))
        
        # 1st Drouput
        model.add(Dropout(self.params['dropout_rate'], name='Dropout_1'))
        
        # 1st MaxPooling
        model.add(MaxPooling2D(pool_size=(1, 15), strides=(1, 15), name='MaxPooling_1'))
        
        # Spatial convolution
        model.add(Conv2D(filters=8, kernel_size=(self.n_channels, 1), activation=self.params['activation'],
                         kernel_initializer=self.params['initializer'],
                         kernel_regularizer=regularizers.l2(1e-4), 
                         name='Spatial_convolution'))
        
        # 2nd Drouput
        model.add(Dropout(self.params['dropout_rate'], name='Dropout_2'))
        
        # 2nd MaxPooling
        model.add(MaxPooling2D(pool_size=(1, 15), strides=(1, 15), name='MaxPooling_2'))
        
        # Flat
        model.add(Flatten())
        
        # Dense
        model.add(Dense(16, activation=self.params['activation'], kernel_initializer=self.params['initializer'],
                        kernel_regularizer=regularizers.l2(1e-4), 
                        name='Dense_1'))
        
        # 3rd Drouput
        model.add(Dropout(self.params['dropout_rate'], name='Dropout_3'))
        
        # Softmax (sigmaid function if binary classification problem)
        if (n_classes>2) :
            model.add(Dense(n_classes, activation = 'softmax', name='Softmax'))
            model.compile(loss = self.params['multiclass_loss'], optimizer=self.params['optimizer'], 
                       metrics=self.params['metrics'])
        else :
            model.add(Dense(1, activation='sigmoid', name='Softmax'))
            model.compile(loss = self.params['binary_loss'], optimizer=self.params['optimizer'],
                       metrics=self.params['metrics'])
        
        self.model = model
        
        
    # Function to fit the model
    def fit_base_model(self, X_train, y_train) :
        self.network_history = self.model.fit(X_train, y_train, epochs=300, verbose=0, use_multiprocessing=True, batch_size=32,
                               validation_split=0.3, callbacks=[self.params['early_stop'], self.params['checkpoint']]) #, self.params['adaptative_lr']])
    
        
    # Function to predict results
    def predict(self, X_test) :
        return self.model.predict_classes(X_test)
        #return self.model.predict_proba(X_test)
    
    # Function to plot CNN training history
    def plot_history(self):
        # Loss
        loss_hist = plt.figure()
        plt.xlabel('Epochs')
        plt.ylabel('Loss')
        plt.plot(self.network_history.history['loss'])
        plt.plot(self.network_history.history['val_loss'])
        plt.legend(['Training', 'Validation'], loc='upper right')
        plt.title('CNN Loss')
        plt.show()
    
        # Accuracy
        acc_hist = plt.figure()
        plt.xlabel('Epochs')
        plt.ylabel('Accuracy')
        plt.plot(self.network_history.history['accuracy'])
        plt.plot(self.network_history.history['val_accuracy'])
        plt.legend(['Training', 'Validation'], loc='lower right')
        plt.title('CNN Accuracy')
        
        plt.show()
        
        return loss_hist, acc_hist
        
       
# Scheduler for adaptative learning rate
def scheduler(epoch, lr):
    if (epoch < 20) :
        return lr
    else:
        return lr * tensorflow.math.exp(1e-2)
        