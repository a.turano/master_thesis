# Module to run the pipeline

from preprocessing.preprocess_data import preprocess_epochs
from preprocessing.split_dataset import split_dataset
from preprocessing.read_raws import read_data
from features_selection.extract_correlation import load_filtered_channels, extract_channel_list
from features_extraction.compute_features import extract_spectral_features, normalize_features
from features_selection.features_reduction import features_reduction
from classification.run_classifiers import run_classifiers
from classification.plot_results import plot_save_results


# Experimental pipeline to run
def run_pipeline(dataset, subjects, runs, classification_pb, channel_filtering_method, 
                 results, save_models, results_single=None) :    
    # Filtering frequencies
    f_min, f_max = .5, 30.
    # Normalize raw EEG data
    zscore_norm = True
    # Normalize spectral features with bandpower normalization 
    # (to classify rest trials)
    bp_norm = False
    # List of filtered channels
    filtered_channels = []
    plot_filtered_data = False
    # Plot average correlation matrix
    plot_corr_mtx = False
    # Dimensionality reduction technique (PCA/LDA)
    features_reduction_technique = None #'LDA' # 
    plot_features_reduction = False
    # Plot confusion matrix
    print_confmat = False
    
    # Add also motor channels
    m_ch_corr = False
    
    if (channel_filtering_method == 'spearman_motor'):
        channel_filtering_method = 'spearman'
        m_ch_corr = True
    
    if (channel_filtering_method == 'pearson_motor'):
        channel_filtering_method = 'pearson'
        m_ch_corr = True    
        
    # Read data from runs
    epochs = []
    
    # Physionet dataset
    if (dataset=='Physionet') :
        # Sampling frequency (Hz)
        samp_freq = 160
        # Extract raw data
        for r in runs :
            raws = read_data(dataset, subjects, r)
            # Preprocess data and extract epochs
            epochs.append(preprocess_epochs(dataset, raws, f_min, f_max, 
                                            zscore_norm, plot_filtered_data))
    # BCI competition 4 dataset (2A - 2B)
    else :
        # Sampling frequency (Hz)
        samp_freq = 250
        # Extract raw data
        raws = read_data(dataset, subjects)
        # Preprocess data and extract epochs
        epochs.append(preprocess_epochs(dataset, raws, f_min, f_max,
                                        zscore_norm, plot_filtered_data, runs))
    
    # Flat epoch list: task_1 rest  (e.g. epochs = [epochs_task_2, epochs_task_3] )
    epochs = [e for sublist in epochs for e in sublist]
    
    # Selecting channels
    filtered_channels = load_filtered_channels(runs, classification_pb,
                                               channel_filtering_method)
    
    # Corr mtx + motor ch
    if (m_ch_corr) :
        filtered_channels += ['C3', 'C4', 'Cz']
        if (channel_filtering_method == 'spearman'):
            channel_filtering_method = 'spearman_motor'
            
        if (channel_filtering_method == 'pearson'):
            channel_filtering_method = 'pearson_motor'
        
    
    # If channel list is empty, extract channels according to a specific
    # channel filtering method
    if (not filtered_channels) :
        filtered_channels = extract_channel_list(channel_filtering_method, epochs, 
                                                 plot_corr_mtx)

    # Extracting spectral features (PSD, avg_band_area)
    data = extract_spectral_features(dataset, epochs, f_min, f_max, samp_freq, 
                                     filtered_channels, bp_norm)
    
    # Split into training and test set
    X_train, X_test, y_train, y_test = split_dataset(data, len(subjects), 
                                                     dataset, random_state=3)
    
    # Data standardization
    X_train, X_test = normalize_features(X_train, X_test, filtered_channels, bp_norm)
    
    # Features reduction
    X_train, X_test, y_train, y_test = features_reduction(X_train, X_test, 
                                                          y_train, y_test, 
                                                          features_reduction_technique,
                                                          plot_features_reduction)
    
    # Classification (predicted_labels = [model, labels])
    predicted_labels = run_classifiers(X_train, X_test, y_train, y_test, 
                                       save_models, dataset, classification_pb,
                                       channel_filtering_method,
                                       random_state=3)
    
    # Save results in dict, print confusion matrices and print accuracy
    plot_save_results(y_test, predicted_labels, results, channel_filtering_method, 
                      subjects, results_single, print_confmat)
