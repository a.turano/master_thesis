# Module to run the pipeline

from preprocessing.preprocess_data import preprocess_epochs
from preprocessing.split_dataset import epochs_to_dataset
from preprocessing.read_raws import read_data
from features_selection.extract_correlation import load_filtered_channels, extract_channel_list
from classification.plot_results import print_conf_mtx, compute_accuracy
from classification.ensemble_model import EnsembleModel
from sklearn.metrics import confusion_matrix
import numpy as np


# Experimental pipeline to run
def run_pipeline(dataset, subjects, runs, classification_pb, channel_filtering_method, 
                 results, save_models, results_single=None) :    
    # Filtering frequencies
    f_min, f_max = .5, 30.
    # Normalize raw EEG data
    zscore_norm = True
    # List of filtered channels
    filtered_channels = []
    plot_filtered_data = False
    # Plot average correlation matrix
    plot_corr_mtx = False

    # Read data from runs
    epochs = []
    # Physionet dataset
    if (dataset=='Physionet') :
        # Sampling frequency (Hz)
        samp_freq = 160
        # Extract raw data
        for r in runs :
            raws = read_data(dataset, subjects, r)
            # Preprocess data and extract epochs
            epochs.append(preprocess_epochs(dataset, raws, f_min, f_max, 
                                            zscore_norm, plot_filtered_data))
    # BCI competition 4 dataset (2A - 2B)
    else :
        # Sampling frequency (Hz)
        samp_freq = 250
        # Extract raw data
        raws = read_data(dataset, subjects)
        # Preprocess data and extract epochs
        epochs.append(preprocess_epochs(dataset, raws, f_min, f_max,
                                        zscore_norm, plot_filtered_data, runs))
    
    # Flat epoch list: task_1 rest  (e.g. epochs = [epochs_task_2, epochs_task_3] )
    epochs = [e for sublist in epochs for e in sublist]
    
    # Selecting channels
    filtered_channels = load_filtered_channels(runs, classification_pb,
                                               channel_filtering_method)
    # If channel list is empty, extract channels according to a specific
    # channel filtering method
    if (not filtered_channels) :
        filtered_channels = extract_channel_list(channel_filtering_method, epochs, 
                                                 plot_corr_mtx)

    # Get training and test set from epochs list
    X_train, X_test, y_train, y_test = epochs_to_dataset(epochs, random_state=3)
    
    # Define and build the ensemble model
    ensemble = EnsembleModel(pred_weights={'frontal_weight':0.1, 'temporal_weight':0.2, 
                                    'central_weight':0.6, 'occipital_weight':0.1})
    
    ensemble.build_ensemble_model(channels_list=filtered_channels, 
                               n_samples=X_train.shape[2], # number of sampling points for each trial
                               n_classes=len(set(y_test))
                               )
        
    # Fit the ensemble
    ensemble.fit_ensemble(X_train, y_train)
    
    # Test the model
    y_predicted = ensemble.predict(X_test=X_test[..., np.newaxis])
    
    # Confusion matrix
    conf_mtx = confusion_matrix(y_test, y_predicted)
    print_conf_mtx(conf_mtx, 'Ensemble')
    
    # Accuracy
    accuracy = compute_accuracy(conf_mtx)
    print('Ensemble_accuracy = '+str(accuracy))
