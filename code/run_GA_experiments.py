# Module to run the GA pipeline


from features_selection.extract_correlation import load_filtered_channels
from classification.run_classifiers import run_classifiers
from classification.plot_results import plot_save_results
import pandas as pd
import scipy.io
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler#, StandardScaler


# Experimental pipeline to run
def run_pipeline(dataset, subjects, runs, classification_pb, channel_filtering_method, 
                 results, save_models, results_single=None) :    
    
    # List of filtered channels
    filtered_channels = []
    # Plot correlation matrix
    plot_corr_mtx = False
    # Plot confusion matrix
    print_confmat = False
    
    # Load features Dataset
    data = pd.read_csv('mi_real_LRH.csv')
    # Load selected features (GA)
    selected_features = scipy.io.loadmat('selected_features_on_avg.mat')
    selected_features = selected_features['idx']
    selected_features = np.concatenate((selected_features, np.array([1]).reshape([1,1])), axis=1)
    # Selected features indices
    idx = np.where(selected_features[:, :] == 1)[1]
    # Select features (GA)
    data = data.iloc[:, idx]
    
    # Selecting channels (Pearson or Spearman correlation)
    filtered_channels = load_filtered_channels(runs, classification_pb,
                                               channel_filtering_method)
    # If the list is empty, load Motor Channels 
    # (we do not take into account the case 'all channels')
    if (not filtered_channels) :
       filtered_channels = ['Cz', 'C3', 'C4'] 
    # Select data only for the specific channel subset of the experiment
    # (using regular expression to select columns)
    regex = 'target'
    for ch in filtered_channels :
        regex += '|' + ch
    data_filtered = data.filter(regex=regex)
    
    # Dataset Splitting
    X_train, X_test, y_train, y_test = train_test_split(data_filtered.loc[:, data_filtered.columns != 'target'],
                                                        data_filtered['target'], test_size=0.3, random_state=3)
    
    # Cast labels to categorical data
    y_train[y_train=='LH'] = 0
    y_train[y_train=='RH'] = 1
    y_train = y_train.astype("category")
    
    y_test[y_test=='LH'] = 0
    y_test[y_test=='RH'] = 1
    y_test = y_test.astype("category")
    
    # Select features (GA)
    #X_train, X_test = X_train.iloc[:, idx], X_test.iloc[:, idx]
    
    # Scale data to 0-1 range
    scaler = MinMaxScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.fit_transform(X_test)
    
    # Classification (predicted_labels = [model, labels])
    predicted_labels = run_classifiers(X_train, X_test, y_train, y_test, 
                                       save_models, dataset, classification_pb,
                                       random_state=3)
    
    # Save results in dict, print confusion matrices and print accuracy
    plot_save_results(y_test, predicted_labels, results, channel_filtering_method, 
                      subjects, results_single, print_confmat)
