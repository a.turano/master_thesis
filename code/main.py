# Loading modules

import numpy as np
import run_CNN_LSTM_experiments
import run_CNN_experiments
import run_deep_experiments
import run_experiments
import run_GA_experiments
#from run_experiments import *#run_pipeline
from classification.save_results import *
import sys


# Function to change classification problem name (to save results) Physionet dataset
def class_pb_Physionet(runs) :
    pb = ''
    if (runs == [[3,7,11],[4,8,12]]) :
        pb += 'multiclass_hands'
    elif (runs == [[5,9,13],[6,10,14]]) :
        pb += 'multiclass_feet'
    elif (runs == [[3,7,11]]) :
        pb += 'MM_hands'
    elif (runs == [[4,8,12]]) :
        pb += 'MI_hands'
    elif (runs == [[5,9,13]]) :
        pb += 'MM_feet'
    elif (runs == [[6,10,14]]) :
        pb += 'MI_feet'
    
    return pb


# Function to change classification problem name (to save results) BCI_2A dataset
def class_pb_BNCI_2A(runs) :
    pb = ''
    if (len(runs) > 2) :
        pb += 'multiclass'
    else :
        for r in runs :
            if (r == 1) :
                pb += 'l'
            if (r == 2) :
                pb += 'r'
            if (r == 3) :
                pb += 'f'
            if (r == 4) :
                pb += 't'
    return pb

# Selecting parameters for experiments:
#   - Dataset to use: Physionet, BCI_2A, BCI_2B
dataset = 'BCI_2A' # 'Physionet' # 


# Physionet subjects
if (dataset == 'Physionet') :
    #   - selecting subjects
    subjects = np.arange(1, 110)
    # subjects 88, 89, 92, 100 have a different sampling frequency (128.0)
    subjects_to_remove =  np.array([88, 89, 92, 100]) - 1
    subjects = np.delete(subjects, subjects_to_remove)
    '''
    ##################################################################
    # runs (movements to classify)
    
    runs = [1]             # baseline (eyes opened)
    runs = [2]             # baseline (eyes closed)
    runs = [3, 7, 11]      # motor execution: left vs right fist
    runs = [4, 8, 12]      # motor imagery: left vs right fist
    runs = [5, 9, 13]      # motor execution: both fists vs both feet
    runs = [6, 10, 14]     # motor imagery: both fists vs both feet
    
    ##################################################################
    '''
    # Selecting runs
    runs_tot = [[[3,7,11]], [[4,8,12]], 
               [[5,9,13]], [[6,10,14]], 
               [[3,7,11],[4,8,12]],
               [[5,9,13],[6,10,14]]
               ]
    
    
# BCI_2A subjects
elif (dataset == 'BCI_2A') :
    subjects = np.arange(1, 10)
    '''
    ##################################################################
    # runs (movements to classify)
    
    runs = [1]             # motor imagery: left fist
    runs = [2]             # motor imagery: right fist
    runs = [3]             # motor imagery: both feet
    runs = [4]             # motor imagery: tongue
    
    ##################################################################
    '''
    runs_tot = [[1,2], [1,3], [1,4], [2,3], 
                [2,4], [3,4], [1,2,3,4]]
        
    
# BCI_2B subjects
elif (dataset == 'BCI_2B') :
    # Subject 9 cannot be loaded correctly
    subjects = np.arange(1, 9)
    '''
    ##################################################################
    # runs (movements to classify)
    
    runs = [1]             # motor imagery: left fist
    runs = [2]             # motor imagery: right fist
    
    ##################################################################
    '''
    runs_tot = [[1, 2]]
    
    
# Error
else :
    sys.exit('UNSUPPORTED DATASET')


#   - type of experiments (group/single)
experiments = 'group'
#   - channel filtering method: motor_ch, pearson, spearman
#     otherwise all channels are selected
channel_filtering_method = ['motor_ch']#, 'all', 'spearman', 'pearson', 'pearson_motor', 'spearman_motor'
#   - save results
save_results = True


# Iterate along different runs
for runs in runs_tot :
    # Get classification problem
    if (dataset == 'BCI_2A') :
        classification_pb = class_pb_BNCI_2A(runs)
    elif (dataset == 'Physionet') :
        classification_pb = class_pb_Physionet(runs)
    else :
        classification_pb = 'hands_im'
    # Initialization of results dictionary
    results = {ch : None for ch in channel_filtering_method}
    for ch in channel_filtering_method :
        results[ch] = {m : 0 for m in ['svm', 'knn', 'mlp']}
       
    # Run group experiments
    if (experiments == 'group') :
        # Run experiments for each channel_filtering_method
        for ch_method in channel_filtering_method :
            # Save only models referring to experiments that pick all channels
            #if (ch_method != 'all') :
            #    save_models = True #False
            #else :
            save_models = True
            # Select the pipeline to run:
            # - run_pipeline(...) -> experiments with PSD features
            # - run_CNN_experiments -> experiments with 'base_model' (CNN)
            # - run_deep_experiments -> experiments with 'ensemble_model' (multiple CNNs)
            # - run_CNN_LSTM_experiments -> experiments with 'CNN_LSTM'
            # - run_GA_experiments -> experiments with features selected by GA
            run_CNN_LSTM_experiments.run_pipeline(dataset, subjects, runs, classification_pb, ch_method, 
                         results, save_models)
        # Save results in a csv file
        if (save_results) :
            save_group_results(results, dataset, experiments, classification_pb,
                               channel_filtering_method)
    
    # Run single subject experiments
    elif (experiments == 'single') :
        # Save models only for group experiments
        save_models = False
        # Initialization of results dictionary for single subjects experiments
        results_single = {  'sbj'       : [],
                            'ch_sel'    : [],
                            'model'     : [],
                            'accuracy'  : []
                            }
        # Run experiments for each subject
        for s in subjects :
            # Run experiments for each channel_filtering_method
            for ch_method in channel_filtering_method :
                # Select the pipeline to run:
                # - run_pipeline(...) -> experiments with PSD features
                # - run_CNN_experiments -> experiments with 'base_model' (CNN)
                # - run_deep_experiments -> experiments with 'ensemble_model' (multiple CNNs)
		        # - run_CNN_LSTM_experiments -> experiments with 'CNN_LSTM'        
                run_CNN_experiments.run_pipeline(dataset, [s], runs, classification_pb, ch_method, 
                             results, save_models, results_single)
        # Save results in a csv file
        if (save_results) :
            # Single results
            save_single_results(results, dataset, experiments, classification_pb, results_single)
            
    # Error
    else :
        sys.exit('INVALID TYPE OF EXPERIMENT')
