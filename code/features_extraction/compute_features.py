# Compute features

import mne
import numpy as np
import pandas as pd
from scipy.integrate import simps  # area under main frequencies
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split


# Define frequency bands
def select_frequency_bands() :
    frequencies = [
                   #('theta', 3, 7),
                   ('alpha', 7, 12),
                   ('beta' , 12, 30),
                   ]
    # Frequency bands
    bands = {f[0] for f in frequencies}
    return frequencies, bands


# Initialize dictionary to save spectral features
def init_features_dict(frequencies, bands, selected_ch) :
    # Dict keys
    keyList = list({b+'_Pavg_'+ch for b in bands for ch in selected_ch})
    {keyList.append(b+'_Pstd_'+ch) for b in bands for ch in selected_ch}
    {keyList.append(b+'_area_'+ch) for b in bands for ch in selected_ch}
    keyList.sort()
    keyList.append('label')
    
    return {key : None for key in keyList}


# Compute PSD using Welch's method
def compute_psd(dataset, epochs, f_min, f_max, samp_freq, selected_ch) :
    #
    if (dataset=='Physionet') :
        t_min, t_max = 0.0, 4.0
    else :
        t_min, t_max = 2.0, 6.0
    # Defining window length (seconds): take a window sufficiently long to 
    # include at least two full cycles of the lowest frequency of interest
    l_window = 2 / f_min
    # points per time segment
    nps = int(l_window * samp_freq)
    
    # Computing PSDS using Welch method (0.5 overlap):
    # psds <- [n_instances, n_channels, n_points]
    # freq <- vector of frequencies on which psds is computed
    psds, freqs = mne.time_frequency.psd_welch(epochs, f_min, f_max,
                                               tmin=t_min, tmax=t_max,
                                               n_per_seg=nps, 
                                               n_fft=nps, picks=selected_ch, 
                                               n_overlap=int(np.floor(.5*nps)), 
                                               average='mean')
    return psds, freqs


# Compute average bandpower of a frequency band
def compute_average_bandpower(psds) :
    # psds <- [n_instances, n_channels, n_points]
    # Mean/std computed between 'n' frequency points
    return psds.mean(axis=2), psds.std(axis=2)


# Compute average area under a specific frequency band
def compute_band_area(psds, freq_res) :
    return simps (psds, dx=freq_res)


# Normalize PSD features
def normalize_bandpower(normalized_data, data_tmp, selected_ch, bands) :
    for ch in selected_ch :
        norm_fact = 0
        # Compute normalization factor for each band
        for b in bands :
            norm_fact += data_tmp[b+'_Pavg_'+ch].mean()
        norm_fact = 1 / norm_fact
        # Normalize bandpower for each band of channel 'ch'
        for b in bands :
            normalized_data[b+'_Pavg_'+ch] = normalized_data[b+'_Pavg_'+ch].values * norm_fact
            
    return normalized_data


# Normalize average area features
def normalize_band_area(data) :
    # min-max normalization
    return (data-min(data)) / (max(data)-min(data))


# Features normalization
def normalize_spectral_features(normalized_data, selected_ch, bp_norm) :
    # Define scaler to normalize features
    scaler = MinMaxScaler()
    # Select frequency bands
    _, bands = select_frequency_bands()
    # Normalized features
    data_tmp = normalized_data.copy()
    
    # Normalizing average area under band 'b' (min-max normalization)
    for ch in selected_ch :
        for b in bands :
            x = np.reshape(data_tmp[b+'_area_'+ch].values, 
                           (data_tmp[b+'_area_'+ch].values.shape[0], 1))
            x = scaler.fit_transform(x)
            normalized_data[b+'_area_'+ch] = np.reshape(x, (1, x.shape[0])).flatten()
            
    # Normalizing average bandpower features for each channel
    if (bp_norm) :
        # Bandpower normalization
        normalized_data = normalize_bandpower(normalized_data, data_tmp, selected_ch, bands)
    else :
        # "Zero_mean/unitary_std" normalization
        for ch in selected_ch :
            for b in bands :
                x = np.reshape(data_tmp[b+'_Pavg_'+ch].values, 
                               (data_tmp[b+'_Pavg_'+ch].values.shape[0], 1))
                x = scaler.fit_transform(x)
                normalized_data[b+'_Pavg_'+ch] = np.reshape(x, (1, x.shape[0])).flatten()
                
                
                x = np.reshape(data_tmp[b+'_Pstd_'+ch].values, 
                               (data_tmp[b+'_Pstd_'+ch].values.shape[0], 1))
                x = scaler.fit_transform(x)
                normalized_data[b+'_Pstd_'+ch] = np.reshape(x, (1, x.shape[0])).flatten()
                
    return normalized_data


# Compute avg bandpower for each band/channel and avg area under each band
def compute_spectral_features(dataset, epochs, label, f_min, f_max, samp_freq, selected_ch):
    # Define frequency bands
    frequencies, bands = select_frequency_bands()
    # Initialize dict of features
    features_dict = init_features_dict(frequencies, bands, selected_ch)
    # Labeling
    features_dict['label'] = np.full(epochs._data.shape[0], label)
    
    # Extract PSD using Welch's method
    psds, freqs = compute_psd(dataset, epochs, f_min, f_max, samp_freq, selected_ch)
    # Frequency resolution
    freq_res = freqs[1] - freqs[0] 
    
    # Compute features for each frequency band
    for band, low_f, high_f in frequencies :
        # Compute average/std bandpower for the frequency band of interest
        # avg_power <- [n_instances, n_channels]
        avg_power, std_power = compute_average_bandpower(psds[:, :, (freqs>=low_f) & (freqs<high_f)])
        # Store features for each channel in dict
        for i, ch in enumerate(selected_ch) :
            features_dict[band+'_Pavg_'+ch] = avg_power[:,i]
            features_dict[band+'_Pstd_'+ch] = std_power[:,i]
            # Compute average area under each band
            features_dict[band+'_area_'+ch] = compute_band_area(psds[:, i, (freqs>=low_f) & (freqs<high_f)],
                                                                freq_res)
    return features_dict


# Compute spectral features and returns a DataFrame containing the extracted features
def extract_spectral_features(dataset, epochs, f_min, f_max, samp_freq, filtered_channels, bp_norm) :
    # Dict of features
    features_dict = {}
    # Computing features for each class
    for label, e in enumerate(epochs) :
        features_dict[label] = (compute_spectral_features(dataset, e, label, 
                                                          f_min, f_max, samp_freq, 
                                                          filtered_channels))  
    # Extracting features for each label
    dataset = {k : None for k in features_dict[0].keys()}
    labels = np.arange(len(epochs))
    for k in dataset.keys() :
        tmp = []
        for l in labels :
            tmp = np.concatenate([tmp, features_dict[l][k]])
        dataset[k] = tmp
    
    return dataset

    
# Normalize spectral features
def normalize_features(X_train, X_test, filtered_channels, bp_norm) :
    # Normalize the training set
    X_train_norm = normalize_spectral_features(X_train, filtered_channels, bp_norm)
    # Normalize the test set
    X_test_norm = normalize_spectral_features(X_test, filtered_channels, bp_norm)
    
    return X_train_norm, X_test_norm
    