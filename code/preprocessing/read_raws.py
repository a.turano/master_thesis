# Read module

from mne.datasets import eegbci         # Physionet dataset
from moabb.datasets import BNCI2014001  # BCI competition 4-2A dataset
from moabb.datasets import BNCI2014004  # BCI competition 4-2B dataset
from mne.io import read_raw_edf


# Defining function to read data from Physionet dataset
def read_Physionet_data(subjects, runs, raws) :
    for s in subjects:
        # List of .edf files
        files = eegbci.load_data(s, runs)
        # Read runs for each subject
        raws += [read_raw_edf(f, preload=True) for f in files]
        
    return raws


# Defining function to read data from BCI competition 2A dataset
def read_BNCI4_2A_dataset(subjects, raws) :
    data = BNCI2014001().get_data(list(subjects))
    # Dataset: 9 subjects -> 2 session for each sbj -> 6 runs for each session
    for sbj in subjects :
        sbj_data = data[sbj]
        for session in sbj_data.keys() :
            # List of raw data
            raws += [sbj_data[session][run] for run in sbj_data[session].keys()]
        
    return raws


# Defining function to read data from BCI competition 2A dataset
def read_BNCI4_2B_dataset(subjects, raws) :
    data = BNCI2014004().get_data(list(subjects))
    # Dataset: 9 subjects -> 5 session for each sbj -> 1 run for each session
    for sbj in subjects :
        sbj_data = data[sbj]
        for session in sbj_data.keys() :
            # List of raw data
            raws += [sbj_data[session][run] for run in sbj_data[session].keys()]

    return raws


# Define funtion to call dataset loader
def read_data(dataset, subjects, runs=None) :
    if (dataset == 'Physionet') :
        return read_Physionet_data(subjects, runs, raws=[])
    elif (dataset == 'BCI_2A') :
        return read_BNCI4_2A_dataset(subjects, raws=[])
    else :
        return read_BNCI4_2B_dataset(subjects, raws=[])
