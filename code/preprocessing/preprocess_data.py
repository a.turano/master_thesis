# Preprocessing module

from mne.datasets import eegbci     # Physionet dataset
from moabb.datasets import BNCI2014001, BNCI2014004  # BCI competition 4 2A, 2B datasets
from mne.io import concatenate_raws
from mne import pick_types
import numpy as np
import mne
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler


# Z-score normalization
def z_score_normalization(data) :
    scaler = StandardScaler()
    data = data.transpose()
    data = scaler.fit_transform(data)
    data = data.transpose()
    return data


# Read epochs from preprocessed data
def extract_Physionet_epochs(raw) :
    # Extract events from raw data
    events, event_ids = mne.events_from_annotations(raw, event_id='auto')
    # Epoching data: n_events * n_channels * n_time points
    tmax = 4.0  # event duration
    # Dividing epochs by task id (event id=1 -> rest)
    e_2 = mne.Epochs(raw, events, event_id=2, tmax=tmax, preload=True) # Task 1
    e_3 = mne.Epochs(raw, events, event_id=3, tmax=tmax, preload=True) # Task 2
    return [e_2, e_3]


def extract_BNCI_epochs(raw, runs) :
    # Sampling frequency
    samp_freq = 250
    # Define new mapping for events
    events = mne.find_events(raw, stim_channel='stim', output='onset')
    # BNCI2014001 (4 classes)
    if (len(set(events[:, 2])) == 4) :
        mapping = {1: 'Left_hand', 2: 'Right_hand', 3: 'Feet', 4: 'Tongue'}
        # Event time
        t_min, t_max = BNCI2014001().interval     # 2s - 6s
    # BNCI2014004 (2 classes)
    else :
        mapping = {1: 'Left_hand', 2: 'Right_hand'}
        # Event time
        t_min, t_max = BNCI2014004().interval
    # Set new mapping
    annot_from_events = mne.annotations_from_events(
        events=events, event_desc=mapping, sfreq=samp_freq,
        orig_time=raw.info['meas_date'])
    raw.set_annotations(annot_from_events)
    # Extract events from raw data
    events, event_ids = mne.events_from_annotations(raw, event_id='auto')
    # Pick only EEG channels
    picks = pick_types(raw.info, eeg=True, stim=False, eog=False, exclude='bads')
    # Epoching data: n_events * n_channels * n_time points
    # Dividing epochs by task id
    epochs = []
    for e_id in runs :
        # e_id = [1, 2, 3, 4] -> [Left hand, Right hand, feet, tongue]
        e = mne.Epochs(raw, events, event_id=e_id, tmax=t_max, preload=True, picks=picks)
        # Cut first two seconds of motor imagery (rest) 
        # -> 250Hz * 2s = 500samples
        #e._data = e._data[:, :, 500:-1]
        epochs.append(e)
    return epochs


# Extract epochs from raw data
def preprocess_epochs(dataset, raws, f_min, f_max, z_score, plot_filtered_data, runs=None) :
    # For each run filter and normalize data
    for r in np.arange(len(raws)):
        # Bandpass filter ( [f_min-f_max] Hz )
        raws[r] = raws[r].filter(l_freq=f_min, h_freq=f_max, picks=['eeg'])
        # Normalization using z-score: X_n = (X-mean)/std
        if (z_score) :
            # Physionet dataset has no stim channel to exclude
            if (dataset == 'Physionet') :
                raws[r]._data = z_score_normalization(raws[r]._data)
            # Exclude 'stim' channel to correctly extract annotations
            else :
                raws[r]._data[0:-1, :] = z_score_normalization(raws[r]._data[0:-1, :])
    
    # Concatenating filtered raws
    raw = concatenate_raws(raws)
    
    # Setting 1005 standard montage: MNE does not support 1010 standard montage
    if (dataset == 'Physionet') :
        # Set channel names
        eegbci.standardize(raw)
    montage = mne.channels.make_standard_montage('standard_1005')
    raw.set_montage(montage)
    # Strip channel names of "." characters
    raw.rename_channels(lambda x: x.strip('.'))
    
    # Plot filtered data
    if (plot_filtered_data) :
        raw.plot(title="Filtered EEG data", scalings=dict(eeg=20e-1))
        raw.plot_psd()
        plt.title("PSD")
        raw.plot_psd(average=True)
        plt.title("Average Filtered PSD")
    
    # Extract epochs using events id from different datasets
    if (dataset == 'Physionet') :
        return extract_Physionet_epochs(raw)
    else :
        return extract_BNCI_epochs(raw, runs)