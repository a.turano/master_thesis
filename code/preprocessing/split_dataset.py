# Module to split the dataset into train and test set


import random
import numpy as np
from sklearn.model_selection import train_test_split
import pandas as pd


# Define function to take 2 test instances for each class (subject-based study)
def train_test_split_subj_based(X, y, inst_for_class, random_state) :
    # Selecting 'inst_for_class' random instances for each class,
    # for each run to build the test set
    rnd = []
    # For each label sample randomly 'inst_for_class' instances
    for l in set(y) :
        sampling_batch = list(y[y==l].index)       
        # Partition of the sampling batch: for each run we take 2 instances
        # per class (3 runs -> 3 partition)
        part = [round(len(sampling_batch)/3), 
                round(len(sampling_batch)*2/3), 
                len(sampling_batch)-1
                ]        
        # Sampling from the 1st run
        random.seed(random_state)
        rnd.append(random.sample(list(np.arange(sampling_batch[0], 
                                sampling_batch[part[0]])), inst_for_class))
        # Sampling from the 2nd run
        random.seed(random_state*2)
        rnd.append(random.sample(list(np.arange(sampling_batch[part[0]], 
                                sampling_batch[part[1]])), inst_for_class))
        # Sampling from the 3rd run
        random.seed(random_state*3)
        rnd.append(random.sample(list(np.arange(sampling_batch[part[1]], 
                                sampling_batch[part[2]])), inst_for_class))    
    # Flattening rnd list
    rnd = [item for sublist in rnd for item in sublist]
    # Build training set and test set
    X_train = X.drop(rnd)
    y_train = y.drop(rnd)
    X_test  = X.loc[rnd]
    y_test  = y.loc[rnd]
    
    return X_train, X_test, y_train, y_test


# Define function to split dataset according to the experiment type
def split_dataset(data, n_sbj=None, dataset=None, random_state=3) :
    '''
    # Subject-based experiments for Physionet dataset
    if ((n_sbj == 1) & (dataset=='Physionet')) :
        inst_for_class = 2
        return train_test_split_subj_based(df.loc[:, df.columns != 'label'], 
                                           df['label'], inst_for_class, 
                                           random_state)
    '''

    # Group-based experiments
    #else :
    df = pd.DataFrame(data=data)
    df["label"] = df["label"].astype("category")
    
    # Return splitted dataset
    return train_test_split(df.loc[:, df.columns != 'label'],
                            df['label'], test_size=0.3, 
                            random_state=random_state)


# Extract dataset from a list of epochs
def epochs_to_dataset(epochs, random_state) :
    for l, e in enumerate(epochs) :
        # Get data and labels from each epoch list (type of movement)
        if (l == 0) :
            data = e.get_data()
            labels = np.array(np.full((len(data), 1), l))
        else :
            data = np.concatenate((data, e.get_data()), axis=0)
            labels = np.concatenate((labels, np.full((len(e), 1), l)), axis=0)
        
    #df = pd.DataFrame(data=data)
    # Cast labels to categorical data type
    #df["label"] = df["label"].astype("category")
    labels = labels.flatten()
    
    # Return splitted dataset
    return train_test_split(data, labels, test_size=0.3, 
                            random_state=random_state)
