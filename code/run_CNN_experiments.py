# Module to run the pipeline

from preprocessing.preprocess_data import preprocess_epochs
from preprocessing.split_dataset import epochs_to_dataset
from preprocessing.read_raws import read_data
from features_selection.extract_correlation import load_filtered_channels, extract_channel_list
from classification.plot_results import print_conf_mtx, compute_accuracy
from classification.save_results import save_model
from classification.base_model import BaseModel
from sklearn.metrics import confusion_matrix
import numpy as np
import os
import matplotlib.pyplot as plt

# Experimental pipeline to run
def run_pipeline(dataset, subjects, runs, classification_pb, channel_filtering_method, 
                 results, save_models, results_single=None) :    
    # Filtering frequencies
    f_min, f_max = 7., 30.
    # Normalize raw EEG data
    zscore_norm = True
    # List of filtered channels
    filtered_channels = []
    plot_filtered_data = False
    # Plot average correlation matrix
    plot_corr_mtx = False
    # Plot confusion matrix
    print_confmat = False
    
    # Add also motor channels
    m_ch_corr = False
    
    if (channel_filtering_method == 'spearman_motor'):
        channel_filtering_method = 'spearman'
        m_ch_corr = True
    
    if (channel_filtering_method == 'pearson_motor'):
        channel_filtering_method = 'pearson'
        m_ch_corr = True
    
    # Read data from runs
    epochs = []
    
    # Physionet dataset
    if (dataset=='Physionet') :
        # Sampling frequency (Hz)
        samp_freq = 160
        # Extract raw data
        for r in runs :
            raws = read_data(dataset, subjects, r)
            # Preprocess data and extract epochs
            epochs.append(preprocess_epochs(dataset, raws, f_min, f_max, 
                                            zscore_norm, plot_filtered_data))
    # BCI competition 4 dataset (2A - 2B)
    else :
        # Sampling frequency (Hz)
        samp_freq = 250
        # Extract raw data
        raws = read_data(dataset, subjects)
        # Preprocess data and extract epochs
        epochs.append(preprocess_epochs(dataset, raws, f_min, f_max,
                                        zscore_norm, plot_filtered_data, runs))
    
    # Flat epoch list: task_1 rest  (e.g. epochs = [epochs_task_2, epochs_task_3] )
    epochs = [e for sublist in epochs for e in sublist]
    
    # Selecting channels
    filtered_channels = load_filtered_channels(runs, classification_pb,
                                               channel_filtering_method)
    # If channel list is empty, extract channels according to a specific
    # channel filtering method
    if (not filtered_channels) :
        filtered_channels = extract_channel_list(channel_filtering_method, epochs, 
                                                 plot_corr_mtx)

    # Get training and test set from epochs list
    X_train, X_test, y_train, y_test = epochs_to_dataset(epochs, random_state=3)
    
    # Get path
    path = os.getcwd() + "/../Results/" + dataset + "/" + classification_pb + "/" + channel_filtering_method
    
    # Corr mtx + motor ch
    if (m_ch_corr) :
        filtered_channels += ['C3', 'C4', 'Cz']
        path += "_motor_ch"
    
    # Define and build the CNN model (using base model class)
    cnn = BaseModel(filtered_channels, path)
    cnn.build_base_model(n_samples=X_train.shape[2],
                           n_classes=len(set(y_test))
                           )
    cnn.model.summary()
    
    if (channel_filtering_method != 'all') :
        
        filtered_idx = []
        for c in filtered_channels :
            filtered_idx.append(epochs[0].ch_names.index(c))
            
        X_train = X_train[:, filtered_idx, :]
        X_test = X_test[:, filtered_idx, :]
        
    # Fit the model
    cnn.fit_base_model(X_train=X_train[..., np.newaxis], 
                         y_train=y_train)
         
    # Test the model
    y_predicted = cnn.predict(X_test=X_test[..., np.newaxis])
    
    # Save model
    if (save_models) :
        save_model(cnn, 'CNN', dataset, classification_pb)
    
    # Confusion matrix
    conf_mtx = confusion_matrix(y_test, y_predicted)
    fig = print_conf_mtx(conf_mtx, 'CNN')
    fig.savefig(path+'/CNN_confmtx.png')
    
    # Plot CNN history
    loss_hist, acc_hist = cnn.plot_history()
    loss_hist.savefig(path+'/CNN_loss_hist.png')
    acc_hist.savefig(path+'/CNN_acc_hist.png')
    
    
    # Accuracy
    accuracy = compute_accuracy(conf_mtx)
    #print('CNN_accuracy = '+str(accuracy))
    file = open(path+'/CNN_acc.txt', 'w')
    file.write('Accuracy = ' + str(accuracy))
    file.close()
    