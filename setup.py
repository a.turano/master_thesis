import setuptools

with open("README", 'r') as f:
    long_description = f.read()


setuptools.setup(
    name='masater_thesis',
    version='0.1.0',
    author='Andrea Turano',
    author_email='andretura95@gmail.com',
    license='GNU GENERAL PUBLIC LICENSE - Version 3, 29 June 2007',
    description='EEG analysis pipeline',
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=[

        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        "Programming Language :: Python :: 3",
        'Topic :: Scientific/Engineering :: Brain Computer Interface',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    platforms=[
    'Environment :: Console',
    'Operating System :: POSIX :: Linux',
    'Operating System :: Microsoft :: Windows :: Windows 10',
    ],
    url='https://gitlab.com/a.turano/master_thesis',
    packages=setuptools.find_packages(),
    install_requires=[
        'numpy',
        'mne',
        'moabb',
        'sklearn',
        'matplotlib',
        'seaborn',
        'scipy',
        'joblib',
        'pandas',
        'tensorflow',
    ],
)